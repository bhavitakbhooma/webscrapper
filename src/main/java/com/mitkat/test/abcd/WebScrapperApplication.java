package com.mitkat.test.abcd;

// Only needed if scraping a local File.
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WebScrapperApplication {
	
	public static Set<String> uniqueURL = new HashSet<String>();
	public static String website;
	public static String nation;
	public static String world;	
	public static String south;
	private static String facebook;
	private static String twitter;
	private static String html;
	private static String entertainment;
	private static String technology;
	private static String sports;
	private static String lifestyle;
	private static String gallery;
	private static String business;
	private static String opinion;
	private static String hash;
	private static String india;
	private static String metros;
	
	
	
	

	public static void main(String[] args) {
		SpringApplication.run(WebScrapperApplication.class, args);
		
		WebScrapperApplication obj1 = new WebScrapperApplication();
	
		
		website = "https://www.deccanchronicle.com";

		 nation = "/nation";
		 world = "/world";
		 south = "/south";
		 india = "/india";
		 metros = "/metros";
		 facebook = "facebook";
		 twitter = "twitter";
		 html = ".html";
		 entertainment = "entertainment";
		 sports = "sports";
		 technology = "technology";
		 lifestyle = "lifestyle";
		 gallery = "gallery";
		 business = "business";
		 opinion = "opinion";
		 hash = "#";
		
		 long currenttime = System.currentTimeMillis();		 
		obj1.get_links("https://www.deccanchronicle.com");	
		long finaltime= System.currentTimeMillis();
		 
		 System.out.println(finaltime-currenttime);
				
	}
		
	
	
	 private void get_links(String url) {
	
		 
	     DateTimeFormatter customFormatter = DateTimeFormatter.ofPattern("MMM dd, yyyy, HH:mm a z");

		try {
			
			Document doc = Jsoup.connect(url).get();
			Elements links = doc.select("a[href]");
			
		      // get the page title (URL title)
		      String title = doc.title();
		      String description = doc.select("meta[name=description]").get(0).attr("content");
		     
		      String dateModified = "";
		      if(doc.select("meta[property=article:modified_time]") != null && doc.select("meta[property=article:modified_time]").size() >0 )
		     {  dateModified   = doc.select("meta[property=article:modified_time]").get(0).attr("content");}
		     
		      String datePosted = "";
		     if(doc.select("meta[property=article:published_time]") != null && doc.select("meta[property=article:published_time]").size() >0)
		     {  datePosted = doc.select("meta[property=article:published_time]").get(0).attr("content");
		     
		     String arr[]=datePosted.split(" ");
		     
		     String month = arr[0];
		     String date = arr[1];
		     String year = arr[2];
		     String time = arr[3];
		     String ampm = arr[4].toUpperCase();
		     String zone = arr[5];
		     
		     String arr1[]= time.split(":");
		     
		
		     
		     String hours = arr1[0];
		    
		     String arr2[] = date.split(",");
		     
		     if(Integer.valueOf(arr2[0]) <= 9)
		     {date = "0"+date;}	 
		    	 
		    	 
		     if(Integer.valueOf(hours) >= 12)
		     {
		    	 hours ="00";
		     }	 
		     
		     else if(Integer.valueOf(hours)>=0 && Integer.valueOf(hours) <= 9)
		     {
		    	 
		    	 hours = "0"+hours;
		     }
		     
		     
		     if(ampm.contentEquals("PM"))
		     {
		    	 hours = String.valueOf((Integer.valueOf(hours)+12));
		     }	 
		     
		     
		     
		   
		     
		     String inputtime = month+ " "+ date +" "+year+ " "+ hours+":"+arr1[1]+" "+ampm+" "+zone;
		     
		    
		     try {
		     LocalDateTime x = LocalDateTime.parse(inputtime, customFormatter);
		     Instant instant = x.toInstant(ZoneOffset.ofHoursMinutes(5, 30));
		     
		     System.out.println("Instant: "+ instant);
		     }
		     catch(Exception e)
		     {e.printStackTrace();}
		     }
		     
  
		    
		    
		     String image = "";
		     if(doc.select("meta[property=og:image]")!= null && doc.select("meta[property=og:image]").size() >0 )
		     {	image = doc.select("meta[property=og:image]").get(0).attr("content");}
		
	    
		      System.out.println("Title: " + title);
		      System.out.println("Description: " + description);
		      System.out.println("Image link: " + image);
		      System.out.println("Date Published: " + datePosted);
		      System.out.println("Date Updated: " + dateModified);
		      
//	      
			
			if (links.isEmpty()) {
	               return;
	            }
			
			
			
			
			 links.stream().parallel().map((link) -> link.attr("abs:href")).forEachOrdered((this_url) -> {
				 
	                boolean add = uniqueURL.add(this_url);
	                
	                if (add && this_url.contains(website) && !this_url.contains(hash) && this_url.contains(html) && !this_url.contains(twitter)&& !this_url.contains(facebook) && !this_url.contains(technology)&& !this_url.contains(opinion)&& !this_url.contains(entertainment)) {
	                	
	                	
	                	if(add && this_url.contains(nation)) {
	                		
	                		System.out.println();
	                		System.out.println(this_url);
	                		get_links(this_url);
	                		 
	                		
	                	}
	                	
	                	else if(add && this_url.contains(world)) {
	                		 System.out.println();
	                		System.out.println(this_url);
	                		get_links(this_url);	
	                		
	                	}
	                	
	                	else if(add && this_url.contains(south)) {
	                		 System.out.println();
	                		System.out.println(this_url);
	                		get_links(this_url);	
	                		
	                	}

	                	else if(add && this_url.contains(india)) {
	                		 System.out.println();
	                		System.out.println(this_url);
	                		get_links(this_url);	
	                		
	                	}
	                	else if(add && this_url.contains(metros)) {
	                		 System.out.println();
	                		System.out.println(this_url);
	                		get_links(this_url);	
	                		
	                	}
	                	
	                }
			 }
			 
			 
			 );
			
			 
			   
		
		     
		}
		
			
			 catch (IOException e) {
					System.out.println("An error occurred.");
				    e.printStackTrace();
				    }
	 }
}

			
			
	

